//var all_map;//全局地图
var cqCenterArr = [106.53, 29.35];//重庆中心
var cqArr;//重庆区域图层

function showMap(options){

    var defaults = {
        id : '',
        levels : [ 1, 13],
        zoom : 7,
        isDefault : true,
        center_lon: 106.53,
        center_lat: 29.35,
        allmapno:'',
        attribution:false
    }

    var opts = $.extend(defaults, options);// 覆盖新参数
    var projection = new ol.proj.Projection({
        code : 'EPSG:4326',
        units : 'degrees',
    });
    var center = [ opts.center_lon, opts.center_lat ];
    var z = opts.zoom;
    if (!z) {
        z = opts.levels[0];
    }

    var map = new ol.Map({
        target : opts.id,
        controls: new ol.control.defaults({
            attribution: opts.attribution//false不显示ol的图标链接
        }),
        view : new ol.View({
            zoom : z,
            minZoom : opts.levels[0],
            maxZoom : opts.levels[1],
            projection : projection,
            center : center
        }),

        layers : [
//							new ol.layer.Tile({
//								source : new ol.source.OSM(),
//							}),
            new ol.layer.Tile(
                {
                    source : new ol.source.TileArcGISRest(
                        {
                            url: 'http://172.24.176.167:6080/arcgis/rest/services/Maps/StreetMap/MapServer'
//													url: 'http://172.24.176.167:6080/arcgis/rest/services/Maps/China_Community_BaseMap/MapServer'
//													url : 'http://cache1.arcgisonline.cn/arcgis/rest/services/ChinaOnlineStreetWarm/MapServer'
                        })
                }) ]
    });

    //all_map = map;
    return map;
}

function remove(options) {
    if(cqArr){
        options.map.removeOverlay(cqArr);
    }
}

function setCenter(map){
    map.getView().setCenter(cqCenterArr);
}

/**
 * 画多边形
 * @param arr      多边形经纬度
 * @param pageArr  vectorLayer
 * @param color    颜色
 * @param map      地图
 * @param lineDash 线条图案
 */
function drawPolygon(arr, color, map, lineDash){

    remove({map:map});

    if(!arr){
        return;
    }

    if(!color){
        color = "blue";
    }

    var geojsonObject = {
        "type": "Feature",
        "geometry": {
            "type": "LineString",
            "coordinates": arr
        },
        "properties": {
            "name": "test"
        }
    }

    function areaLineStyle(feature) {
        var p = {//样式
            width: 1,
            color: color
        };
        if(lineDash){
            p.lineDash = lineDash;
        }
        var sk = new ol.style.Stroke(p);
        var style = new ol.style.Style({
            stroke: sk
        })
        return style;
    }

    var vectorSource = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

    var vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: areaLineStyle
    });

    map.addOverlay(vectorLayer);

    cqArr = vectorLayer;

}

function getImgOpts(options) {
    // 默认参数
    var defaults = {
        url : '',
        opacity : 0.7,
        visible : true,
        extent : [ 108.361, 29.03, 116.45, 33.257 ],
        zIndex : 9,
        callback : function(img) {
        }
    }
    var opts = $.extend(defaults, options);// 覆盖新参数

    function imageLoadFunction(){

    }

    var extent = [0, 0, 1024, 968];
    var projection = new ol.proj.Projection({
        code: 'xkcd-image',
        units: 'pixels',
        extent: extent
    });

    var imageStatic = new ol.source.ImageStatic({
        url : opts.url,
        imageExtent : opts.extent,
        //imageLoadFunction: imageLoadFunction,
        projection:projection
    });
    var img = new ol.layer.Image({
        source : imageStatic,
        opacity : opts.opacity,//透明[0,1]默认为1
        visible : opts.visible,//能见度默认0或1，1可见
        extent : opts.extent,//图层渲染的边界范围
        //zIndex : opts.zIndex,//渲染前对图层进行排序
        //minResolution: 480*272,//图片最小分辨率
        //maxResolution: 4096*2160,//图片最大分辨率
    });
    opts.img = img;
    return opts;
}

function addImg(opts){
    opts.map.addLayer(opts.img);
}
